import groovyx.net.http.*
import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.TEXT
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import tagcloud.Tag

@Grab(group='org.codehaus.groovy.modules.http-builder',
    module='http-builder', version='0.5.2' )

def tags = retrieveTags()
writeToFile( "../../web/tags.json", tags )

def retrieveTags() {
	def stateData = []
	def stateList = [
		AL:"Alabama",
		AK:"Alaska",
		AZ:"Arizona",
		AR:"Arkansas",
		CA:"California",
		CO:"Colorado",
		CT:"Connecticut",
		DE:"Delaware",
		FL:"Florida",
		GA:"Georgia",
		HI:"Hawaii",
		ID:"Idaho",
		IL:"Illinois",
		IN:"Indiana",
		IA:"Iowa",
		KS:"Kansas",
		KY:"Kentucky",
		LA:"Louisiana",
		ME:"Maine",
		MD:"Maryland",
		MA:"Massachusetts",
		MI:"Michigan",
		MN:"Minnesota",
		MS:"Mississippi",
		MO:"Missouri",
		MT:"Montana",
		NE:"Nebraska",
		NV:"Nevada",
		NH:"New Hampshire",
		NJ:"New Jersey",
		NM:"New Mexico",
		NY:"New York",
		NC:"North Carolina",
		ND:"North Dakota",
		OH:"Ohio",
		OK:"Oklahoma",
		OR:"Oregon",
		PA:"Pennsylvania",
		RI:"Rhode Island",
		SC:"South Carolina",
		SD:"South Dakota",
		TN:"Tennessee",
		TX:"Texas",
		UT:"Utah",
		VT:"Vermont",
		VA:"Virginia",
		WA:"Washington",
		WI:"Wisconsin",
		WV:"West Virginia",
		WY:"Wyoming"]
	def slurper = new JsonSlurper()
	def http = new HTTPBuilder( 'http://gomashup.com' )

	stateList.each { state ->
		http.request( GET, TEXT ) {
			uri.path = '/json.php'
			uri.query = [ fds:"geo/usa/zipcode/state/${state.key}" ]

			response.success = { resp, reader ->
				def json = unpadJSON( reader.text )
				def data = slurper.parseText( json )

				stateData << new Tag(title: state.value, salience: data.result.size())
			}
		}
	}

	Collections.shuffle( stateData )
	return stateData
}

String unpadJSON( jsonp ) {
	def json = jsonp.substring( jsonp.indexOf('(')+1, jsonp.lastIndexOf(')') )

	return json
}

def writeToFile( filePath, stateData ) {
	def json = new JsonBuilder(stateData)

	new File( filePath ).withWriter { out ->
		out.writeLine( json.toString() )
	}
}