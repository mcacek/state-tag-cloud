var stateData = [{"title":"Vermont","salience":308},{"title":"California","salience":2678},{"title":"Alabama","salience":847},{"title":"Indiana","salience":991},{"title":"Minnesota","salience":1036},{"title":"Tennessee","salience":805},{"title":"Nevada","salience":227},{"title":"North Carolina","salience":1087},{"title":"Louisiana","salience":731},{"title":"North Dakota","salience":418},{"title":"Rhode Island","salience":91},{"title":"New Hampshire","salience":281},{"title":"Utah","salience":352},{"title":"Maryland","salience":619},{"title":"Iowa","salience":1071},{"title":"Alaska","salience":269},{"title":"Georgia","salience":967},{"title":"Oregon","salience":485},{"title":"South Dakota","salience":416},{"title":"New Jersey","salience":737},{"title":"Wyoming","salience":197},{"title":"Ohio","salience":1472},{"title":"Florida","salience":1470},{"title":"Idaho","salience":332},{"title":"Kansas","salience":772},{"title":"Colorado","salience":662},{"title":"Delaware","salience":98},{"title":"Nebraska","salience":628},{"title":"Arkansas","salience":720},{"title":"Virginia","salience":1257},{"title":"Kentucky","salience":1016},{"title":"Maine","salience":505},{"title":"New York","salience":2233},{"title":"Hawaii","salience":143},{"title":"South Carolina","salience":538},{"title":"Arizona","salience":515},{"title":"New Mexico","salience":427},{"title":"Montana","salience":411},{"title":"Wisconsin","salience":913},{"title":"Texas","salience":2682},{"title":"Washington","salience":716},{"title":"Connecticut","salience":436},{"title":"Oklahoma","salience":778},{"title":"Mississippi","salience":538},{"title":"West Virginia","salience":930},{"title":"Missouri","salience":1192},{"title":"Michigan","salience":1167},{"title":"Massachusetts","salience":713},{"title":"Illinois","salience":1596},{"title":"Pennsylvania","salience":2226}]
	, cloud = APP.TagCloud.create( stateData )	
	, first = stateData[0]
	, eighteenth = stateData[17]
	, last = stateData[49]
	, sortedStateData = cloud.sortTags( stateData.slice() );

test( "Vermont is first before the sort.", function() {
	ok( stateData[0].title === 'Vermont', "Vermont is not the first state!" );
});

test( "Sorted state data.", function() {
	ok( sortedStateData[0].title === 'Alabama', "Alabal is the 1st state." );
	ok( sortedStateData[17].title === 'Louisiana', "Louisiana is the 18th state." );
	ok( sortedStateData[49].title === 'Wyoming', "Wyoming is the 49th state." );
});
// Test max salience
test( "Get max salience.", function() {
	ok( cloud.getMaxSalience() === 2682, "Max salience is 2682." );
});

// Test get salience class
test( "Get salience classes.", function() {
	ok( cloud.getSalienceClass(0,10) == 'salience0', "salience0" );
	ok( cloud.getSalienceClass(1,10) == 'salience1', "salience1" );
	ok( cloud.getSalienceClass(2,10) == 'salience2', "salience2" );
	ok( cloud.getSalienceClass(3,10) == 'salience3', "salience3" );
	ok( cloud.getSalienceClass(4,10) == 'salience4', "salience4" );
	ok( cloud.getSalienceClass(5,10) == 'salience5', "salience5" );
	ok( cloud.getSalienceClass(6,10) == 'salience6', "salience6" );
	ok( cloud.getSalienceClass(7,10) == 'salience7', "salience7" );
	ok( cloud.getSalienceClass(8,10) == 'salience8', "salience8" );
	ok( cloud.getSalienceClass(9,10) == 'salience9', "salience9" );
	ok( cloud.getSalienceClass(10,10) == 'salience10', "salience10" );
});