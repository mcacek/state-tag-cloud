( function ( APP, $ ) { 
	APP.TagCloud = function() {
		var exports = {};

		exports.create = function( rawTagData ) {
			var exports = {}
				, salienceClassPrefix = 'salience'
				, uniqueId = 1;

			// Private functions 
			function fadeOutTags( elements ) {
				$( elements ).fadeOut( 'slow' );
			}

			function fadeInTags( elements ) {
				$( elements ).fadeIn( 'slow' );
			}

			function sortComparator( el1, el2 ) {
				if ( el1.title < el2.title ) {
					return -1;
				}
				else if ( el1.title > el2.title ) {
					return 1;
				}

				return 0;
			}

			function addEventHandlers() {
				$( '#emptyCloudBtn' ).click( emptyCloudBtnClick );
				$( '#drawCloudBtn' ).click( drawCloudBtnClick );
				$( '#searchTxt' ).keyup( searchTxtKeyup );
			}

			function addUniqueIds() {
				var counter = 0,
					length = rawTagData.length;

				for ( ; counter < length; counter++ ) {
					rawTagData[counter].id = getUniqueId();
				}
			}

			function getUniqueId() {
				return uniqueId++;
			}

			function emptyCloudBtnClick() {
				exports.empty();
			}

			function drawCloudBtnClick() {
				exports.drawCloud( true );
			}

			function searchTxtKeyup() {
				exports.searchAndHighlight( this.value );
			}

			// Public functions
			exports.empty = function() {
				fadeOutTags( $('.cloud-tag') );
				$('.cloud-content').empty();
			};

			exports.getMaxSalience = function() {
				var max = 0
					, current;

				for (tagKey in rawTagData) {
					current = rawTagData[tagKey].salience;
					if ( current > max) {
						max = current;
					}
				}

				return max;
			}

			exports.getSalienceClass = function( salience, max ) {
				var classNum = Math.round( (salience/max) *10 );
				return salienceClassPrefix +classNum;
			}

			exports.sortTags = function( unsortedTagData ) {
				return unsortedTagData.sort( sortComparator );
			};

			exports.drawCloud = function( doSort ) {
				var el
					, tagData = rawTagData
					, tag
					, $content = $('<div/>')
					, fontSize
					, maxSalience = this.getMaxSalience()
					, $el
					, counter = 0;			

				if ( doSort ) {
					// always pass a copy to sort so that we can get back to unsorted tags
					tagData = this.sortTags( tagData.slice() );
				}

				for (tagKey in tagData) {
					tag = tag = tagData[tagKey];;
					$el = $( '<span />' );
					$el.addClass( 'cloud-tag' )
						.attr( 'title', tag.salience + ' postal codes' )
						.addClass( this.getSalienceClass(tag.salience, maxSalience) )
						.html( tag.title )
						.attr( 'data-tag-id', tag.id );

					$content.append( $el );
				}

				$( '.cloud-content' ).html( $content.html() );
				fadeInTags( $('.cloud-tag') );
				$( '.cloud-content' ).tooltip();
			};

			exports.searchAndHighlight = function( searchInput ) {
				this.clearHighlights();

				if ( searchInput ) {
					var results = this.searchCloud( searchInput );
					this.highlightResults( results );
				}
			};

			exports.searchCloud = function( searchInput ) {
				var counter = 0
					, length = rawTagData.length
					, results = [];
				
				for ( ; counter < length; counter++ ) {
					if ( rawTagData[counter].title.toLowerCase().indexOf( searchInput.toLowerCase() ) > -1 ) {
						results.push( rawTagData[counter] );
					}
				}

				return results;
			};

			exports.highlightResults = function( tags ) {
				var counter = 0
					, length = tags.length;

				for ( ; counter < length; counter++ ) {
					$( '[data-tag-id=' +tags[counter].id +']' ).addClass( 'is-highlighted' );
				}
			};

			exports.clearHighlights = function() {
				$( '.cloud-tag' ).removeClass( 'is-highlighted' );
			};


			addUniqueIds();
			$('#searchTxt').focus();
			addEventHandlers();

			return exports;
		};

		return exports;
	}();
} ( APP, jQuery ) );