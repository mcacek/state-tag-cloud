$.ajax( {
	url: '/tags.json',
	success: function( data ) {
		$(function() {
			APP.cloud = APP.TagCloud.create( data );
			 APP.cloud.drawCloud( true );
		});
	},
	error: function() {
		alert("System error encountereed.  Please try again.");
		throw new Error("Data source '/tags/json.' does not exist!");
	}
} );